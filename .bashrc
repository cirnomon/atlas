#
# ~/.bashrc
#


# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color'
alias ll='ls -lah'
PS1='\e[1;33m[ʎ \W]\e[m '
. "$HOME/.cargo/env"

export PATH="$HOME/.config/dwm-build/dwmscripts:$PATH"

[[ $(fgconsole 2>/dev/null) == 1 ]] && exec sx dash ~/.xinitrc
