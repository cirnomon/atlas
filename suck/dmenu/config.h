/* See LICENSE file for copyright and license details. */

static char topbar = 1;			/* -b option; if 0, dmenu appears at bottom */
static char centered = 0;		/* -c option; centers dmenu on screen */
static int min_width = 1000;		/* minimum width when centered */
static char fuzzy = 1;			/* -F option; if 0, dmenu doesn't use fuzzy matching */
/* -fn option overrides fonts[0]; default X11 font or font set */
static const char *fonts = "monospace:size=10";


static const char col_soft_black[] = "#282828";
static const char col_gray[] = "#504945";
static const char col_dirty_white[] = "#ebdbb2";
static const char col_green[] = "#427b58";
static const char *prompt = NULL;	/* -p option; prompt to the left of input field */
static const char *colors[SchemeLast][2] = {
	[SchemeNorm] = {col_dirty_white, col_soft_black},	// Normal
	[SchemeSel] = {col_dirty_white, col_gray},	// Selected
	[SchemeSelHighlight] = {col_green, col_soft_black},
	[SchemeNormHighlight] = {col_green, col_gray},
	[SchemeOut] = {col_dirty_white, col_soft_black},
	[SchemeMid] = {col_dirty_white, col_soft_black},
};

static char lines = 0;
static char lineheight = 22;	/* -h option; minimum height of a menu line */
static char columns = 0;

static const char worddelimiters[] = " ";

static unsigned int border_width = 5;	/* -bw option; to add border width */
