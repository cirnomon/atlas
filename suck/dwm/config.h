static const char snap      = 32;       /* snap pixel */
static const char gappih    = 10;       /* horiz inner gap between windows */
static const char gappiv    = 10;       /* vert inner gap between windows */
static const char gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static const char gappov    = 10;       /* vert outer gap between windows and screen edge */
static const char showbar            = 1;        /* 0 means no bar */
static const char topbar             = 1;        /* 0 means bottom bar */
static const char vertpad            = 4;       /* vertical padding of bar */
static const char sidepad            = 4;       /* horizontal padding of bar */
static const char horizpadbar        = 20;        /* horizontal padding for statusbar */
static const char vertpadbar         = 4;        /* vertical padding for statusbar */

static const char *fonts            = "mononoki Nerd Font Mono:style-Medium:size=22";
static const char dmenufont[]       = "mononoki Nerd Font Mono:size=12";
static const char col_gray1[]       = "#282828";
static const char col_gray2[]       = "#504945";
static const char col_gray3[]       = "#bdae93";
static const char col_gray4[]       = "#ebdbb2";
static const char col_cyan[]        = "#cc241d";
static const char col_red[]         = "#9d0006";
static const char col_yellow[]      = "#b57614";
static const char *colors[][3]      = {
	[SchemeNorm] = { col_gray3, col_gray1, col_gray1 },
	[SchemeSel]  = { col_gray4, col_gray1, col_yellow  },
	[SchemeStatus]  = { col_gray1, col_gray1,  col_gray1  },
};

/* tagging */
static const char *tags[] = { "", "", "", "", "" };

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const char nmaster     = 1;    /* number of clients in master area */
static const char resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const char lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#include "vanitygaps.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "[M]",      monocle },
	{ "[@]",      spiral },
};

/* key definitions */
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/dash", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run","-c", "-l", "15", "-m", dmenumon, "-fn", dmenufont, NULL };
static const char *rofipower[]  = { "rofi_power", NULL };
static const char *termcmd[]  = { "st", NULL };
static const char *alsacmd[]  = { "st", "-e", "pulsemixer", NULL };
static const char *newscmd[]  = { "st", "-e", "newsboat", NULL };
static const char *weathercmd[]  = { "st", "-e", "weather", NULL };
static const char *browser[]  = { "firefox", NULL };
static const char *ytcmd[]  = { "yt", "-g", NULL };
static const char *livecmd[]  = { "streams", NULL };
static const char *gamescmd[]  = { "launch_games", NULL };
static const char *editcmd[]  = { "editconfig", NULL };
static const char *htop[]  = { "st", "-e", "htop", NULL };
static const char *manga_cli[]  = { "st", "-e", "manga-cli", NULL };
static const char *ani_cli[]  = { "st", "-e", "ani-cli", NULL };
static const char *raincmd[]  = { "mpv", "--loop-file=inf", "https://www.saisp.br/cgesp/satellite.jpg", NULL };
static const char *calcmd[]  = { "st", "-e", "calcurse", NULL };
static const char *filecmd[]  = { "st", "-e", "nnn", NULL };
static const char *workcmd[]  = { "st", "-d", "w", NULL };
static const char *vimcmd[] = { "st", "-e", "nvim", NULL};

static const char *zapfox[]  = { "firefox", "https://web.whatsapp.com", NULL };
static const char *ytfox[]  = { "firefox", "https://youtube.com", NULL };
static const char *gitfox[]  = { "firefox", "https://github.com", NULL };
static const char *instafox[]  = { "firefox", "https://instagram.com", NULL };
static const char *mailfox[]  = { "firefox", "https://mail.google.com", NULL };
static const char *mastfox[]  = { "firefox", "https://mastodon.gamedev.place/", NULL };



#include "movestack.c"
static const Key keys[] = {
	/* modifier                     key        function        argument */
	{ MODKEY|ControlMask,           XK_w,      spawn,          {.v = zapfox } },
	{ MODKEY|ControlMask,           XK_y,      spawn,          {.v = ytfox } },
	{ MODKEY|ControlMask,           XK_g,      spawn,          {.v = gitfox } },
	{ MODKEY|ControlMask,           XK_i,      spawn,          {.v = instafox } },
	{ MODKEY|ControlMask,           XK_m,      spawn,          {.v = mailfox } },
	{ MODKEY|ControlMask,           XK_t,      spawn,          {.v = mastfox } },
	{ MODKEY,                       XK_d,      spawn,          {.v = dmenucmd } },
	{ MODKEY,                       XK_n,      spawn,          {.v = newscmd } },
	{ MODKEY|ShiftMask,             XK_n,      spawn,          {.v = vimcmd } },
	{ MODKEY,                       XK_r,      spawn,          {.v = raincmd } },
	{ MODKEY,                       XK_c,      spawn,          {.v = weathercmd } },
	{ MODKEY,                       XK_y,      spawn,          {.v = ytcmd } },
	{ MODKEY,                       XK_s,      spawn,          {.v = livecmd } },
	{ MODKEY,                       XK_m,      spawn,          {.v = manga_cli } },
	{ MODKEY|ShiftMask,             XK_a,      spawn,          {.v = alsacmd } },
	{ MODKEY,             XK_a,      spawn,          {.v = ani_cli } },
	{ MODKEY|ShiftMask,             XK_p,      spawn,          {.v = rofipower } },
	{ MODKEY,                       XK_w,      spawn,          {.v = browser } },
	{ MODKEY|ShiftMask,                       XK_w,      spawn,          {.v = workcmd } },
	{ MODKEY,                       XK_g,      spawn,          {.v = gamescmd } },
	{ MODKEY,                       XK_e,      spawn,          {.v = editcmd } },
	{ MODKEY|ShiftMask,                       XK_g,      spawn,          {.v = htop } },
	{ MODKEY,	                XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY,                       XK_f,      spawn,      {.v = filecmd} },
	{ MODKEY,                       XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,                       XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,                       XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,                       XK_o,      incnmaster,     {.i = -1 } },
	{ MODKEY,                       XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,                       XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY|ShiftMask,             XK_j,      movestack,      {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_k,      movestack,      {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_Return, zoom,           {0} },
	{ MODKEY|Mod4Mask,              XK_u,      incrgaps,       {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_u,      incrgaps,       {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_i,      incrigaps,      {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_i,      incrigaps,      {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_o,      incrogaps,      {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_o,      incrogaps,      {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_6,      incrihgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_6,      incrihgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_7,      incrivgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_7,      incrivgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_8,      incrohgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_8,      incrohgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_9,      incrovgaps,     {.i = +1 } },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_9,      incrovgaps,     {.i = -1 } },
	{ MODKEY|Mod4Mask,              XK_0,      togglegaps,     {0} },
	{ MODKEY|Mod4Mask|ShiftMask,    XK_0,      defaultgaps,    {0} },
	{ MODKEY,                       XK_Tab,    view,           {0} },
	{ MODKEY,	                XK_q,      killclient,     {0} },
	{ MODKEY,                       XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY|ShiftMask,                       XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY|ShiftMask,             XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,                       XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,             XK_space,  togglefloating, {0} },
	{ MODKEY,                       XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,             XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,                       XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,                       XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,             XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,             XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_1,                      0)
	TAGKEYS(                        XK_2,                      1)
	TAGKEYS(                        XK_3,                      2)
	TAGKEYS(                        XK_4,                      3)
	TAGKEYS(                        XK_5,                      4)
	TAGKEYS(                        XK_6,                      5)
	{ MODKEY|ShiftMask,             XK_e,      quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

